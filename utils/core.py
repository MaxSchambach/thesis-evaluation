"""
Core utilities.
Definition of metrics, metric formats, metric names, etc.
"""
import hashlib
from pathlib import Path
from tqdm import tqdm
import re
import unicodedata
import urllib.request


PBAR = None

DATA_PATH = Path("./data.h5")

METRICS = ['central_view_mean_absolute_error',
           'central_view_mean_square_error',
           'central_view_psnr',
           'central_view_ssim',
           'central_view_ms_ssim',
           'central_view_cosine_proximity',
           'central_view_spectral_angle',
           'central_view_spectral_information_divergence',
           'disparity_mean_absolute_error',
           'disparity_mean_square_error',
           'disparity_total_variation',
           'disparity_bad_pix_01',
           'disparity_bad_pix_03',
           'disparity_bad_pix_07']

METRICS_NAME_MAPPER = {
    'central_view_mean_absolute_error': "MAE",
    'central_view_mean_square_error': "MSE",
    'central_view_psnr': "PSNR / dB",
    'central_view_ssim': "SSIM",
    'central_view_ms_ssim': "MS-SSIM",
    'central_view_cosine_proximity': "CS",
    'central_view_spectral_angle': "SA / deg",
    'central_view_spectral_information_divergence': "SID",
    'disparity_mean_absolute_error': "MAE / px",
    'disparity_mean_square_error': "MSE / px2",
    'disparity_total_variation': "TV",
    'disparity_bad_pix_01': "BP01 / %",
    'disparity_bad_pix_03': "BP03 / %",
    'disparity_bad_pix_07': "BP07 / %",
}

# Mark smallest value as best value
FRMT_COLS_MIN = {'central_view_mean_absolute_error',
                  'central_view_mean_square_error',
                  'central_view_cosine_proximity',
                  'central_view_spectral_angle',
                  'central_view_spectral_information_divergence',
                  'disparity_mean_absolute_error',
                  'disparity_mean_square_error',
                  'disparity_total_variation',
                  'disparity_bad_pix_01',
                  'disparity_bad_pix_03',
                  'disparity_bad_pix_07'}

# Mark largest value as best value
FRMT_COLS_MAX = {'central_view_psnr',
                 'central_view_ms_ssim',
                 'central_view_ssim'}

# 2 decimal precision
FRMT_COLS_2f = {'central_view_psnr',
                'central_view_ssim',
                'central_view_ms_ssim',
                'central_view_spectral_angle',
                'disparity_total_variation',
                'disparity_bad_pix_01',
                'disparity_bad_pix_03',
                'disparity_bad_pix_07'}

# 4 decimal precision
FRMT_COLS_4f = {'central_view_mean_absolute_error',
                'central_view_mean_square_error',
                'central_view_cosine_proximity',
                'central_view_spectral_information_divergence',
                'disparity_mean_absolute_error',
                'disparity_mean_square_error'}




def _convert_valid(value, allow_unicode=False):
    """Convert an input to a valid filename.

    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    value = re.sub(r'[-\s]+', '-', value).strip('-_')
    return "key_" + value.replace("-", "_")


def data_exists():
    if Path("data.h5").exists():
        print("Found local data file")
        return True

    return False


def data_valid():
    print("Checking file checksum...")
    # File to check
    file_name = 'data.h5'
    original_md5 = '4354313f0c3f214a7b31b261f0e59152'

    try:
        with open(file_name, "rb") as f:
            file_hash = hashlib.md5()
            while chunk := f.read(8192):
                file_hash.update(chunk)
        file_md5 = file_hash.hexdigest()

        if file_md5 == original_md5:
            print("File checksum check successful.")
            return True

    except UnicodeDecodeError:
        print("File incomplete.")

    print("File checksum check fails.")
    return False


class DownloadProgressBar(tqdm):
    """See https://stackoverflow.com/a/53877507"""
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


def retrieve_data():
    if data_exists() and data_valid():
        print("Data available and valid.")
    else:
        print("Downloading data...")
        url = "https://gitlab.com/MaxSchambach/thesis-binaries/-/raw/main/data.h5"
        with DownloadProgressBar(unit='B', unit_scale=True,
                             miniters=1, desc=url.split('/')[-1]) as t:
            urllib.request.urlretrieve(url, "data.h5", reporthook=t.update_to)
        print("done.")


method_mapper = lambda method: _convert_valid(method, allow_unicode=False)
