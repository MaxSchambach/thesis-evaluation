"""
Core utilities.
Definition of metrics, metric formats, metric names, etc.
"""
import ipywidgets as widgets

from .core import METRICS
from .core import METRICS_NAME_MAPPER

from .data_tables import display_table
from .figures import display_figure, display_figure_reconstruction_detail


DATASETS = [
    # Synthetic dataset challenges
    'challenges',
    # Real-world dataset (own)
    'real_own',
    # Real-world dataset by Xiong et al.
    'real_xiong'
]

SCENES = {
    'challenges': ['Bust', 'Cabin', 'Elephant', 'Backgammon', 'Circles', 'Dots'],
    'real_own': ['Floral', 'Floral_250mm', 'Diavolo', 'Diavolo_250mm', 'Wagons', 'Wagons_250mm'],
    'real_xiong': ['Boards', 'Fruits', 'Toys']
}

METHODS = [
       # Compressed sensing methods and subsequent disparity estimation
       'CS 5D-DCT + EPINET',
       'CS Vec.-D. + EPINET',
       'CS Tens.-D. + EPINET',
       # Single-task (ST), Multi-task (MT) and auxiliar loss (AL) methods
       'ST Naive',
       'MT Naive',
       'MT GradNorm',
       'MT Uncertainty',
       'ST AL GradSim',
       'ST AL NormGradSim',
       'MT AL GradSim',
       'MT AL NormGradSim',
       # Combined MT Uncertainty and AL NormGradSim
       'MT Uncertainty + AL',
       # 4D convolution architecture, using MT uncertainty and AL NormGradSim
       '4D Conv.',
       # Different angular resolutions
       '3 x 3',
       '5 x 5',
       '7 x 7',
       '9 x 9',
       # Different coding masks
       'Random',
       'Random macropixel',
       'Regular',
       'Regular optimized'
]


def _get_widget_methods(methods, methods_select):    
    widget = widgets.SelectMultiple(
        options=methods,
        value=methods_select,
        rows=len(methods),
        description='Methods',
        disabled=False)
        
    return widget


def _get_widget_metrics(metrics, metrics_select):        
    widget = widgets.SelectMultiple(
        options=metrics,
        value=metrics_select,
        rows=len(metrics)+1,
        description="Metrics",
        disabled=False)
    return widget

def get_widget_scenes(dataset):
    scenes = SCENES[dataset]
    widget = widgets.Select(
        options= scenes,
        value=scenes[0],
        rows=len(scenes)+1,
        description="Scenes",
        disabled=False)
    return widget


# Define widgets
widget_cv_error = widgets.Checkbox(
    value=False,
    description='Plot central view local error',
    disabled=False,
    indent=True
)

widget_disp_error = widgets.Checkbox(
    value=False,
    description='Plot disparity local error',
    disabled=False,
    indent=True
)

# Create dataset and scene widgets
# When dataset changes, update scene widget
widget_dataset = widgets.Dropdown(
    options=DATASETS,
    value='challenges',
    description='Dataset:',
    disabled=False,
)

widget_scene = get_widget_scenes(widget_dataset.value)

def scene_handler(change):
    global widget_scene
    widget_scene.options = SCENES[change['new']]
    widget_scene.set_state(widget_scene.get_state())
    return

widget_dataset.observe(scene_handler, names='value')


methods_select = ['MT Naive', 
                  'MT Uncertainty', 
                  'MT Uncertainty + AL', 
                  'Regular optimized']

metrics_select_table = ['central_view_psnr', 
                        'central_view_ssim', 
                        'central_view_ms_ssim', 
                        'central_view_spectral_information_divergence', 
                        'central_view_spectral_angle', 
                        'disparity_mean_square_error', 
                        'disparity_mean_absolute_error', 
                        'disparity_bad_pix_01', 
                        'disparity_bad_pix_03', 
                        'disparity_bad_pix_07']

metrics_select_plot = ['central_view_psnr', 
                       'central_view_ssim',
                       'central_view_spectral_angle', 
                       'disparity_mean_square_error',
                       'disparity_bad_pix_07']

widget_metrics_table = _get_widget_metrics(METRICS, metrics_select_table)
widget_methods_table = _get_widget_methods(METHODS, methods_select)
widget_metrics_plot = _get_widget_metrics(METRICS, metrics_select_plot)
widget_methods_plot = _get_widget_methods(METHODS, methods_select)
widget_method_plot_detail = widgets.Dropdown(
                                options=METHODS,
                                value='Regular optimized',
                                description='Method:',
                                disabled=False,
                            )

# Define button widgets for plotting
button_plot = widgets.Button(description="Show plot", indent=True)
output_plot = widgets.Output()
button_plot_detail = widgets.Button(description="Show plot", indent=True)
output_plot_detail = widgets.Output()
button_table = widgets.Button(description="Show table", indent=True)
output_table = widgets.Output()

def _cb_button_plot(b):
    with output_plot:
        output_plot.clear_output()
        print("Creating figure...")
        display_figure(widget_dataset.value, widget_scene.value, ('GT', ) + widget_methods_plot.value, widget_metrics_plot.value, error_cv=widget_cv_error.value, error_disp=widget_disp_error.value)
        print("done.")

def _cb_button_plot_detail(b):
    with output_plot_detail:
        output_plot_detail.clear_output()
        print("Creating figure...")
        display_figure_reconstruction_detail(widget_dataset.value, widget_scene.value, widget_method_plot_detail.value)
        print("done.")

def _cb_button_table(b):
    with output_table:
        output_table.clear_output()
        display_table(widget_methods_table.value, widget_metrics_table.value)

button_plot.on_click(_cb_button_plot)
button_plot_detail.on_click(_cb_button_plot_detail)
button_table.on_click(_cb_button_table)

table_widget = widgets.VBox([widgets.HBox([widget_methods_table, widget_metrics_table]),
                             button_table, output_table])

plot_widget = widgets.VBox([widgets.HBox([widget_dataset,  widget_scene]),
                             widgets.HBox([widget_methods_plot, widget_metrics_plot]),
                             widget_cv_error, 
                             widget_disp_error,
                             button_plot, output_plot])


plot_widget_detail = widgets.VBox([widgets.HBox([widget_dataset,  widget_scene]),
                                   widget_method_plot_detail,
                                   button_plot_detail, 
                                   output_plot_detail])
