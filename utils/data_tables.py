"""
Module to create a table for test dataset performance.
"""
import re

import deepdish as dd
import pandas as pd

from IPython.display import HTML as display_html

from .core import method_mapper
from .core import DATA_PATH
from .core import METRICS, METRICS_NAME_MAPPER
from .core import FRMT_COLS_2f, FRMT_COLS_4f, FRMT_COLS_MIN, FRMT_COLS_MAX


def _build_dataframe(methods):

    data = dd.io.load(DATA_PATH, group="/test")

    # Drop "GT" from methods, there is no GT column for the test dataset.
    methods = [m for m in methods if m != "GT"]

    cols = ['Method'] + METRICS
    df = pd.DataFrame(columns=cols)

    for method in methods:
        try:
            tmp = data[method_mapper(method)]
            del tmp
        except KeyError:
            raise KeyError(f"Unknown method '{method}'.")

        metric_vals = [data[method_mapper(method)]['metrics'][metric] for metric in METRICS]
        df_tmp = pd.DataFrame([[method] + metric_vals], columns=cols)

        df = df.append(df_tmp)

    return df


def _build_table(df, metrics):
    bold_props = "font-weight: bold"
    df = df.loc[:, ['Method'] + metrics]
    df = df.reset_index(drop=True)

    metrics_set = set(metrics)

    styler = df.style
    styler = styler.format(
        formatter="{:.2f}",
        subset=list(metrics_set & FRMT_COLS_2f),
        na_rep='{-}')
    styler = styler.format(
        formatter="{:.4f}",
        subset=list(metrics_set & FRMT_COLS_4f),
        na_rep='{-}')
    styler = styler.highlight_min(
        subset=list(metrics_set & FRMT_COLS_MIN),
        props=bold_props)
    styler = styler.highlight_max(
        subset=list(metrics_set & FRMT_COLS_MAX),
        props=bold_props)
    styler = styler.hide_index()
    styler = styler.set_properties(**{'font-size': 'large'})

    table = styler.to_html()

    # Build new table header
    # Count metric categories
    metrics_cv = [m for m in metrics if "central_view" in m]
    metrics_disp = [m for m in metrics if "disparity" in m]
    num_metrics_cv = len(metrics_cv)
    num_metrics_disp = len(metrics_disp)

    header = '<thead><tr><th></th>'
    if num_metrics_cv > 0:
        header += f'<th colspan="{num_metrics_cv}" style="text-align: center">Central view</th>'
    if num_metrics_disp > 0:
        header += f'<th colspan="{num_metrics_disp}" style="text-align: center">Disparity</th>'

    header += '</tr></thead><thead><tr><th class="col_heading level0 col0" style="text-align: center">Method</th>'

    for i, metric in enumerate(metrics_cv):
        header += f'<th class="col_heading level0 col{i+1}" style="text-align: center">{METRICS_NAME_MAPPER[metric]}</th>'
    for i, metric in enumerate(metrics_disp):
        header += f'<th class="col_heading level0 col{i+1+num_metrics_cv}" style="text-align: center">{METRICS_NAME_MAPPER[metric]}</th>'

    header += '</tr></thead>'

    # Replace old header
    table = re.sub('<thead>.*?</thead>', header, table, flags=re.DOTALL)

    return "<h3 style='text-align:left'>Test dataset performance</h3>"+ table


def get_table(methods, metrics):

    df = _build_dataframe(methods)
    return _build_table(df, metrics)


def display_table(methods, metrics):
    return display(display_html(get_table(list(methods), list(metrics))))
