"""
Module to create a figure for evaluation visualization.
"""
from itertools import product

import copy
import deepdish as dd
import numpy as np
from scipy.ndimage import median_filter

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from plenpy.spectral import SpectralImage

from .colors import set_color_palette, COLORS_DICT

from .core import method_mapper
from .core import DATA_PATH
from .core import FRMT_COLS_2f, FRMT_COLS_4f

# Set matplotlib style
mpl.style.use('./utils/eval.mplstyle')

# Set colormaps
set_color_palette()
CM_DISP_VIRIDIS = copy.copy(mpl.cm.get_cmap("viridis"))
CM_DISP_SPECTRAL = copy.copy(mpl.cm.get_cmap("Spectral_r"))
CM_DISP = CM_DISP_SPECTRAL
CM_ERROR = copy.copy(mpl.cm.get_cmap("Reds"))

# Set NAN color plot to grey
NAN_COLOR = 0.6
CM_DISP.set_bad(color=(NAN_COLOR, NAN_COLOR, NAN_COLOR))
CM_ERROR.set_bad(color=(NAN_COLOR, NAN_COLOR, NAN_COLOR))
LABELPAD_WIDE = 2.5


def format_metric(metric, metric_name):
    if type(metric) == str or type(metric) == np.str_:
        return metric
    if metric_name in FRMT_COLS_2f:
        val = f"{metric:.2f}"
    elif metric_name in FRMT_COLS_4f:
        val = f"{metric:.4f}"
    else:
        raise ValueError(f"Unknown metric name '{metric_name}'.")
    return val


def share_axes(axs, target=None, sharex=False, sharey=False):
    """https://stackoverflow.com/a/51684195"""
    axs = np.asarray(axs)
    if target is None:
        target = axs.flat[0]
    # Manage share using grouper objects
    for ax in axs.flat:
        if sharex:
            target._shared_x_axes.join(target, ax)
        if sharey:
            target._shared_y_axes.join(target, ax)
    # Turn off x tick labels and offset text for all but the bottom row
    if sharex and axs.ndim > 1:
        for ax in axs[:-1,:].flat:
            ax.xaxis.set_tick_params(which='both', labelbottom=False, labeltop=False)
            ax.xaxis.offsetText.set_visible(False)
    # Turn off y tick labels and offset text for all but the left most column
    if sharey and axs.ndim > 1:
        for ax in axs[:,1:].flat:
            ax.yaxis.set_tick_params(which='both', labelleft=False, labelright=False)
            ax.yaxis.offsetText.set_visible(False)
    return


def axis_hide_xticks(ax):
    ax.set_xticks([])
    ax.set_xticklabels([])
    return


def axis_hide_yticks(ax):
    ax.set_yticks([])
    ax.set_yticklabels([])
    return


def axis_hide_ticks(ax):
    axis_hide_xticks(ax)
    axis_hide_yticks(ax)
    return


def axis_hide_all(ax):
    axis_hide_ticks(ax)
    for spine in ['top', 'bottom', 'left', 'right']:
        ax.spines[spine].set_linewidth(0.0)
        ax.spines[spine].set_color("white")
    return


def axis_set_zorder(ax, zorder):
    """ Set the zorder of the axis outlines"""
    for k, spine in ax.spines.items():
        spine.set_zorder(zorder)


def _pad_result_epinet(input, pad_val=np.nan):
    """Pad the disparity or central view from EPINET prediction with an 11px border
    so it has the original spatial resolution."""
    if input.ndim == 3:
        return np.pad(input, ((11, 11), (11, 11), (0, 0)), constant_values=pad_val)
    else:
        return np.pad(input, ((11, 11), (11, 11)), constant_values=pad_val)


def _preprocess_central_view_real(cv,
                                  gamma=0.5,
                                  norm_range=None,
                                  whitebalance=None,
                                  convert_rgb=False,
                                  return_range=False):

    if whitebalance is not None:
        cv /= whitebalance

    if convert_rgb:
        cv = cv.get_rgb()
    if norm_range is None:
        cv_min, cv_max = np.nanmin(cv), np.nanmax(cv)
    else:
        cv_min, cv_max = norm_range
    cv -= cv_min
    cv /= (cv_max - cv_min)
    cv = np.clip(cv, 0.0, 1.0)**gamma

    if return_range:
        return cv, cv_min, cv_max

    return cv


def _get_figure(methods, data, data_meta, data_gt, metrics, error_cv, error_disp):

    # Extract metrics
    metrics_cv = [m for m in metrics if "central_view" in m]
    metrics_disp = [m for m in metrics if "disparity" in m]

    # Calculate figure size
    num_cols = len(methods)
    num_rows = 3

    if error_cv:
        num_rows += 1

    if error_disp:
        num_rows += 1

    # Define figure sizes for known shapes
    figsize = (2.7*len(methods), 2.7*(num_rows-1)+2)

    fig, ax = plt.subplots(num_rows,
                           num_cols,
                           figsize=figsize,
                           gridspec_kw={'height_ratios': (num_rows - 1)*[1.0] + [0.3]})

    share_axes([ax[i, j] for i, j in product(range(num_rows-1), range(num_cols))], sharex=True, sharey=True)

    cv_row = 0
    cv_error_row = cv_row + 1 if error_cv else 0
    disp_row = 1 if not error_cv else 2
    disp_error_row = disp_row + 1 if error_disp else 0
    spectrum_row = max(cv_row, cv_error_row, disp_row, disp_error_row) + 1

    # Set shared ylabels
    ax[cv_row, 0].set_ylabel("Central view", labelpad=LABELPAD_WIDE)
    ax[disp_row, 0].set_ylabel("Disparity", labelpad=LABELPAD_WIDE)
    if error_cv:
        ax[cv_error_row, 0].set_ylabel("Absolute Error", labelpad=LABELPAD_WIDE)
    if error_disp:
        ax[disp_error_row, 0].set_ylabel("Absolute Error", labelpad=LABELPAD_WIDE)
    ax[spectrum_row, 0].set_ylabel("AU")


    ch_ticks = [0, 4, 8, 12]
    ch_ticks_minor = list(range(13))
    ch_ticklabels = [400 + 25*i for i in ch_ticks]
    chs = range(13)

    # Extract global data, needed to plot reference spectra
    cv_gt = SpectralImage(data_gt['central_view'], band_info=data_meta['band_info'])
    cv_gt_rgb, cv_gt_min, cv_gt_max = _preprocess_central_view_real(cv_gt,
                                                                    gamma=data_meta['gamma'],
                                                                    convert_rgb=True,
                                                                    return_range=True)
    # Calculate local error maps
    if error_cv:
        cv_errors = [np.mean(np.abs(data[method_mapper(method)]['central_view'] - data_gt['central_view']), axis=-1) for method in methods]
        cv_error_min = np.nanmin(cv_errors)
        cv_error_max = np.nanmax(cv_errors)
    else:
        cv_errors = [np.nan for method in methods]
        cv_error_min = np.nan
        cv_error_max = np.nan

    if error_disp:
        disp_errors = [np.abs(data[method_mapper(method)]['disparity'] - data_gt['disparity']) for method in methods]
        disparity_error_min = np.nanmin(disp_errors)
        disparity_error_max = np.nanmax(disp_errors)
    else:
        disp_errors = [np.nan for method in methods]
        disparity_error_min = np.nan
        disparity_error_max = np.nan

    # Iterate over data
    for i, method in enumerate(methods):

        cv = SpectralImage(data[method_mapper(method)]['central_view'], band_info=data_meta['band_info'])
        cv_rgb = _preprocess_central_view_real(cv,
                                               norm_range=(cv_gt_min, cv_gt_max),
                                               gamma=data_meta['gamma'],
                                               convert_rgb=True)
        # Replace nan values with grey color
        cv_rgb[np.where(np.isnan(cv_rgb))] = NAN_COLOR
        cv_error = cv_errors[i]
        disp = data[method_mapper(method)]['disparity']
        disp_error = disp_errors[i]
        metrics_curr = data[method_mapper(method)]['metrics']

        ax[0, i].set_title(method)
        ax[cv_row, i].imshow(cv_rgb, interpolation='none')
        axis_hide_all(ax[cv_row, i])


        if error_cv:
            ax[cv_error_row, i].imshow(cv_error, cmap=CM_ERROR, vmin=cv_error_min, vmax=cv_error_max,
                                       interpolation='none')
            axis_hide_all(ax[cv_error_row, i])

        # disp_norm = get_centered_norm(vmin=disparity_range[0], vmax=disparity_range[1])
        # ax[disp_row, i].imshow(disp, cmap=CM_DISP, norm=disp_norm, interpolation='none')
        ax[disp_row, i].imshow(disp, cmap=CM_DISP, vmin=data_meta['disparity_range'][0], vmax=data_meta['disparity_range'][1],
                               interpolation='none')
        axis_hide_all(ax[disp_row, i])

        # Print metrics
        metric_cv_vals = [format_metric(metrics_curr[metric], metric) for metric in metrics_cv]
        metric_disp_vals = [format_metric(metrics_curr[metric], metric) for metric in metrics_disp]
        ax[cv_row, i].set_xlabel(" | ".join(metric_cv_vals))
        ax[disp_row, i].set_xlabel(" | ".join(metric_disp_vals))


        if error_disp:
            ax[disp_error_row, i].imshow(disp_error, cmap=CM_ERROR, vmin=disparity_error_min,
                                         vmax=disparity_error_max, interpolation='none')
            axis_hide_all(ax[disp_error_row, i])

        for x, y in data_meta['spectrum_ref_points']:
            # Plot reference point
            ax[cv_row, i].plot(x, y, 'o')

            # Load and normalize spectra
            spectrum_gt = np.asarray(cv_gt[y, x]).copy()
            # min_gt = np.nanmin(spectrum_gt)
            # spectrum_gt -= min_gt
            max_gt = np.nanmax(spectrum_gt)
            spectrum_gt /= max_gt

            spectrum = np.asarray(cv[y, x]).copy()
            # spectrum -= min_gt
            spectrum /= max_gt

            ax[spectrum_row, i].plot(chs, spectrum_gt, '-', color='grey')
            ax[spectrum_row, i].plot(chs, spectrum, 'o')

        # Plot reference spectrum
        ax[spectrum_row, i].set_xlim(-1, 13)
        ax[spectrum_row, i].set_ylim(0, 1.1)
        ax[spectrum_row, i].set_xticks(ch_ticks)
        ax[spectrum_row, i].set_xticks(ch_ticks_minor, minor=True)
        ax[spectrum_row, i].set_xticklabels(ch_ticklabels)
        ax[spectrum_row, i].set_xlabel("Wavelength in nm")

        ax[spectrum_row, i].set_yticks([])
        ax[spectrum_row, i].set_yticklabels([])

    return fig


def _get_figure_reconstruction_detail(method, data, data_gt, data_meta):
        
    # Prepare GT data
    cv_gt = SpectralImage(data_gt['central_view'], band_info=data_meta['band_info'])
    cv_gt_rgb, cv_gt_min, cv_gt_max = _preprocess_central_view_real(cv_gt,
                                                                    gamma=data_meta['gamma'],
                                                                    convert_rgb=True,
                                                                    return_range=True)
    # Prepare reconstructed data
    cv = SpectralImage(data[method_mapper(method)]['central_view'], band_info=data_meta['band_info'])
    cv_rgb = _preprocess_central_view_real(cv,
                                           norm_range=(cv_gt_min, cv_gt_max),
                                           gamma=data_meta['gamma'],
                                           convert_rgb=True)

    # Specify channels to plot
    channels = [-1,] + list(range(13))

    # Calculate reference values for channel-wise normalization
    cv_gt_min = [float(median_filter(cv_gt[..., ch], size=3).min())for ch in range(13)]
    cv_gt_max = [float(median_filter(cv_gt[..., ch], size=3).max()) for ch in range(13)]

    figsize = (2.7*2, 2.7*13)
    fig, ax = plt.subplots(len(channels), 2, figsize=figsize, sharex=True, sharey=True)


    for cv_curr, cv_rgb_curr, axidx, m in zip([cv_gt, cv], [cv_gt_rgb, cv_rgb], [0, 1], ['GT', 'Reconstructed']):

        for i, ch in enumerate(channels):
            if ch == -1:
                im_kwargs = dict(vmin=0, vmax=1, interpolation='none', aspect='equal')
                img_curr = cv_rgb_curr
                c = "RGB"
            else:
                im_kwargs = dict(vmin=cv_gt_min[ch], vmax=cv_gt_max[ch], interpolation='none', aspect='equal')
                img_curr = np.squeeze(np.asarray(cv_curr[..., ch]))
                # img_curr = img_curr**0.75
                c = f"Channel {ch+1}"

            ax[i, axidx].imshow(img_curr, **im_kwargs)
            
            # Only set title for first axis
            ax[i, 0].set_ylabel(c)

            axis_hide_all(ax[i, axidx])
        ax[0, axidx].set_title(m)

    return fig


def display_figure(dataset, scene, methods, metrics, error_cv=True, error_disp=True):
    plt.close()
    if "real" in dataset:
        methods_curr = ["Ground truth + EPINET" if m == "GT" else m for m in methods]
        key_gt = method_mapper("Ground truth + EPINET")
    else:
        methods_curr = ["Ground truth" if m == "GT" else m for m in methods]
        key_gt = method_mapper("Ground truth")

    ground_truth_group = [f"/{dataset}/{scene}/{key_gt}/central_view",
                          f"/{dataset}/{scene}/{key_gt}/disparity"]

    data_group = [f"/{dataset}/{scene}/{method_mapper(m)}" for m in methods_curr]

    metadata_group = [f"/{dataset}/band_info",
                      f"/{dataset}/gamma",
                      f"/{dataset}/{scene}/disparity_range",
                      f"/{dataset}/{scene}/spectrum_ref_points"]

    # Wrap loaded data into dictionary
    data_gen = lambda group: {key: val for key, val in zip([g.split("/")[-1] for g in group],
                                                           dd.io.load(DATA_PATH, group=group))}

    data = data_gen(data_group)
    data_meta = data_gen(metadata_group)
    data_gt = data_gen(ground_truth_group)

    fig = _get_figure(methods=methods_curr,
                      data=data, data_meta=data_meta, data_gt=data_gt,
                      metrics=metrics,
                      error_cv=error_cv, error_disp=error_disp)
    display(fig.canvas)
    return




def display_figure_reconstruction_detail(dataset, scene, method):
    plt.close()
    methods = ["GT", method]
    if "real" in dataset:
        methods_curr = ["Ground truth + EPINET" if m == "GT" else m for m in methods]
        key_gt = method_mapper("Ground truth + EPINET")
    else:
        methods_curr = ["Ground truth" if m == "GT" else m for m in methods]
        key_gt = method_mapper("Ground truth")

    ground_truth_group = [f"/{dataset}/{scene}/{key_gt}/central_view"]

    data_group = [f"/{dataset}/{scene}/{method_mapper(m)}" for m in methods_curr]

    metadata_group = [f"/{dataset}/band_info",
                      f"/{dataset}/gamma"]

    # Wrap loaded data into dictionary
    data_gen = lambda group: {key: val for key, val in zip([g.split("/")[-1] for g in group],
                                                           dd.io.load(DATA_PATH, group=group))}

    data = data_gen(data_group)
    data_meta = data_gen(metadata_group)
    data_gt = data_gen(ground_truth_group)

    fig = _get_figure_reconstruction_detail(
        method=method, data=data, data_meta=data_meta, data_gt=data_gt)
    display(fig.canvas)
    return